package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int a, int b, int c) {
        if (a + b > c && a + c > b && c + b > a) {
            if (a == b && b == c) {
                return "Равносторонний";
            } else if (a == b || b == c || a == c) {
                return "Равнобедренный";
            } else {
                return "Разносторонний";
            }
        } else {
            return "Треугольник не существует";
        }
    }
    // END
}
