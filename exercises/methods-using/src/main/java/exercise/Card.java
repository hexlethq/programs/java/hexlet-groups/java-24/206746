package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        StringBuilder starsString = new StringBuilder();
        for (int i = 0; i < starsCount; i++) {
            starsString.append("*");
        }
        System.out.println(starsString + cardNumber.substring(cardNumber.length() - 4));
        // END
    }
}
