package exercise;

class Sentence {
    public static void printSentence(String sentence) {
        // BEGIN
        if (sentence.endsWith("!")) {
            System.out.println(String.format("%S", sentence));
        } else {
            System.out.println(sentence.toLowerCase());
        }
        // END
    }
}
