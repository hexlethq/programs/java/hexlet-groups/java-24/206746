package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String phrase) {
        phrase = phrase.trim();
        String abbreviation = ((Character) phrase.charAt(0)).toString();
        for (int i = 0; i < phrase.length(); i++) {
            if (Character.isWhitespace(phrase.charAt(i))) {
                do {
                    i++;
                } while (Character.isWhitespace(phrase.charAt(i)));
                abbreviation += phrase.charAt(i);
            }
        }
        return abbreviation.toUpperCase();
    }
    // END
}
