package exercise;

class Converter {
    // BEGIN
    public static void main(String[] args) {
        System.out.println("10 Kb = " + convert(10, "b") + " b");
    }

    public static int convert(int number, String direct) {
        if ("kb".equals(direct)) {
            return number / 1024;
        } else if ("b".equals(direct)) {
            return number * 1024;
        } else {
            return 0;
        }
    }
    // END
}
